# Matrix bridge for Gancio

First attempt on crossposting to Matrix.

## pieces of the puzzle

What is working:

- send an html message when an event is created in gancio

Things to code / do:
- send poster in a first message before description
- plain text description, without html. gancio has a function for that, may we be able to use it?
- proper logging using the same as gancio
- write a proper README

Things to investigate

- the "client" store. it may grow up and up. i don't think we want this
- the bot sdk. maybe more appropiate than js sdk?
- learn about token lifetime. how to get them. i read that when you close a session, the token gets invalidated. should we ask for user/passwd instead?
- note vs message. bots usually send this other type of message
- now we send to all rooms where we are. how to accept invites for other rooms? related to bot sdk
- bots on encrypted rooms. c'mon, can't we send anyway unencrypted?? it used to be allowed. t2bot has this limitation, they may have info on it.
- how to modify and delete posts. do we need to keep a event-message id dictionary table?


## testing and developing

use the official gancio docker image with the data volume. there inside plugins/ , clone this repo. do npm install. and then go back to data/ and run the docker with docker-compose up.

if some basic error like syntax or undefined vars occur, you can run the script with node to get more detail of the error (line number, etc.). just to node index.js 

if the problem is in runtime, you can open a console and do:

plugin  = requrie("./index.js")
gancio = {}
settings = {...}
plugin.load(gancio, settings)

and then create an event in your local gancio. see what happens ;)


