const sdk = require('matrix-js-sdk');
const nf = require('node-fetch');
global.fetch = nf;

const plugin = {
  configuration: {
    name: 'Matrix',
    author: 'fadelkon',
    url: 'https://framagit.org/bcn.convocala/gancio-plugin-matrix-bridge',
    description: 'Republishes content to Matrix rooms. The goal of this plugin is to make it easier for people in other decentralized and floss networks to reach info from gancio/fediverse',
    settings: {
      bot_address: {
          type: 'TEXT',
          description: 'Bot\'s matrix address',
          hint: 'Address of the existing matrix account that will send the messages, in the form of <pre>@user:server.tld</pre>'
      },
      access_token: {
        type: 'TEXT',
        description: 'Access token',
        required: true,
        hint: 'The <strong>access token</strong> of the you account you will use. In element it\'s hidden at the bottom of the <i>Help & About</i> section of the Settings. There\'s also <a href="https://webapps.stackexchange.com/a/160612">a command line way</a>'
      },/*
      room_id: {
        type: 'TEXT',
        description: 'Room id',
        required: true,
          hint: 'The room\'s "internal" full address. That is, <pre>!jhpZBTbckszblMYjMK:matrix.org</pre> instead of <pre>#test:matrix.org</pre>'
      },*/
      base_url: {
        type: 'TEXT',
        description: 'Matrix server base URL',
        hint: 'The protocol and hostname of the Matrix server where the bot account belongs to. e.g. <pre>https://matrix.org</pre>',
        required: true,
      }
    }
  },
  gancio: null,
  settings: null,
  client: null,
  load(gancio, settings) {
    plugin.gancio = gancio
    plugin.settings = settings
    console.debug(settings)

    // Initialization as for https://matrix.org/docs/guides/usage-of-the-matrix-js-sdk
    plugin.client = sdk.createClient({
        baseUrl: settings.base_url,
        accessToken: settings.access_token,
        userId: settings.bot_address
    });
    plugin.client.startClient();
    plugin.client.once('sync', function(state, prevState, res) {
        console.log(state); // state will be 'PREPARED' when the client is ready to use
    });
  },
  onEventCreate(event) {
    console.log("matrix log")
    if (! event.is_visible) {
       return;
    }
    plugin.client.getRooms().forEach(room => {
        console.log(`sending to room {room.roomId}`)
        plugin.client.sendEvent(room.roomId, "m.room.message", renderEventText(event)).then((res) => {
            console.log("message sent :)")
        }).catch((err) => {
            console.error(err);
        })
    })

  },

  onEventUpdate(event) {
    console.error(`Event "${event.title}" updated`)
    console.error(event)
  },

  onEventDelete(event) {
    console.error(`Event "${event.title}" deleted`)
    console.error(event)
  }
}
function renderEventImage(event) {
    return null
}

function renderEventText(event) {
  let body_html = `\
<h2>${event.title}</h2>

📅 ${(new Date(event.start_datetime * 1000)).toLocaleString()}
📌 ${event.place.name}

${event.description}

${event.tags.reduce((x, y) => { return x + " #" + y.tag.replaceAll(" ", "_") }, "").trim()}
<a href="${plugin.gancio.settings.baseurl}">${plugin.gancio.settings.title}</a>
`
    return {
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
        "formatted_body": body_html,
        "body": `title: ${event.title}`,
    }
}

module.exports = plugin
